## DbStore - Uebung 20
Eine Beispielimplementierung zum DB Store, basierend auf unserem backend template.

Hinweise:
- auf jeden fall muss connect() auf dem DBPool aufgerufen werden, bevor das erste mal ```getInstance()``` aufgerufen wird.
Daher ist es keine gute Idee im constructor eine instanzvariable mit getInstance() zu belegen - das wird aufgerufen bevor der pool connected ist.
Im script wird eine Instanzvariable benutzt, hier wurde aber der connection pool nicht als singleton implementiert sondern von aussen injected (über den constructor),
deshalb funktioniert es.
- wenn man im dbPool env variablen verwenden will, so bietet es sich an alle env vars in eine config auszulagern, 
ansonsten muss man mehrfach dotenv importieren, siehe hierzu app.config.ts
- wenn delete mit einem update des status implementiert wird, so ist darauf zu achten, dass bei einem find/findall/update dieser
status wieder berücksichtigt wird.
- in allen routen die async sind, müssen die exceptions gecatcht werden, ansonsten führt eine exception zum stoppen des servers
    ```javascript
    this.router.get("/fail/:sid", async(req, res, next) => {
         throw new Error("this kills the server")
    });
    
    this.router.get("/nofail/:sid", async(req, res, next) => {
        try {
            throw new Error("this does not");
        } catch(e: any) {
            //next(e) // calls default handler we defined
            // OR
            res.status(500).json({error:"internal"});
        }
    });
    ```
