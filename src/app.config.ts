import * as dotenv from "dotenv";
import assert from "assert";


dotenv.config();
// fail loud, fail early
assert(process.env.PORT,"env - port missing")
assert(process.env.DB_USER,"env - db user missing")
assert(process.env.DB_PWD,"env - db pwd missing")

// central config, to keep all config in one place
export default {
    PORT: parseInt(process.env.PORT as string, 10),
    DB_USER: process.env.DB_USER,
    DB_PWD: process.env.DB_PWD
}