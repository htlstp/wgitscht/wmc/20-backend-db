/**
 * Required External Modules
 */
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import express, {Express} from "express";
import cors from "cors";
import helmet from "helmet";
import {ProductRouter} from "./product/product.router";
import {ProductService} from "./product/product.service";
import {ProductDbStore} from "./product/store/product.db.store";
import {DBPool} from "./dbPool";
import config from "./app.config";


class Application{

    private express:Express;
    constructor() {
        this.express = express();
        this.configureExpress();
    }

    private configureExpress(){
        this.express.use(helmet());
        this.express.use(cors());
        this.express.use(express.json());

        const loggerOptions: expressWinston.LoggerOptions = {
            transports: [new winston.transports.Console()],
            msg: "HTTP {{req.method}} {{req.url}} {{res.statusCode}} response after {{res.responseTime}}ms",
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.prettyPrint(),
                winston.format.colorize({level: true}),
                winston.format.printf(({ level, message, timestamp }) => {
                    return `${timestamp} [${level}]: ${message}`;
                })
            ),
        };
        this.express.use(expressWinston.logger(loggerOptions));

        this.addRoutes();

        // default handler for all undefined routes
        this.express.use((req:any, res:any, next:any) => {
            res.status(404);
            res.json({error:"route not found"});
        });
        // default error handler
        this.express.use((err:any, req:any, res:any, next:any) => {
            console.error(err);
            res.status(500).json({error:"Internal Server Error"}); // don't leak too much info
        });
    }

    private addRoutes(){
        /** Add new Main-Routes here (i.e. 'api/blog', 'api/student', 'ws/message', ...) */
        const productService = new ProductService(new ProductDbStore());
        this.express.use("/api/product", new ProductRouter(productService).router);
    }

    public run(){
        DBPool.connect().then(() => { // this way we wait until the pool is ready before we start the app
            this.express.listen(config.PORT, function () {
                console.log(`Listening on port ${config.PORT}`);
            })
        });
    }
}

new Application().run();