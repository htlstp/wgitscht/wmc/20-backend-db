import {ConnectionPool} from 'mssql';
import config from "./app.config"
export class DBPool {
    private static instance:ConnectionPool | undefined;

    private static config = {
        user: config.DB_USER, // by importing app.config we make sure env is already set
        password: config.DB_PWD,
        server: 'localhost', // You can use 'localhost\\instance' to connect to named instance
        database: 'wmc',
        pool: {
            max: 10,
            min: 0,
            idleTimeoutMillis: 30000
        },
        trustServerCertificate: true
    }

    static getInstance(): ConnectionPool {
        if(!this.instance){
            throw "Pool not connected to DB";
        }
        return this.instance
    }

    static async connect() {
        // it is crucial this is called _before_ using getInstance, we ensure it by waiting for the connection
        // before we listen, see index.ts
        if(this.instance) {
            return; // already connected
        }
        this.instance = await (new ConnectionPool(this.config)).connect();
    }
}