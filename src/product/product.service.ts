import {Product} from "./product";
import {ProductStore} from "./store/product.store";

export class ProductService{
    constructor(private productStore:ProductStore) {
    }

    public async findAll():Promise<Product[]>{
        return await this.productStore.findAll();
    }

    public async find(sid:number):Promise<Product>{
        return await this.productStore.find(sid);
    }

    public async create(product:Product):Promise<Product>{
        if(product.price < 0 || !product.name || product.name.trim().length == 0){
            throw  new Error("product name or price are not valid")
        }
        return await this.productStore.put(product);
    }

    public async update(product:Product):Promise<Product> {
        if(product.price < 0 || !product.name || product.name.trim().length == 0){
            throw  new Error("product name or price are not valid")
        }
        return await this.productStore.update(product)
    }
    /** add new application logic here */
    public async delete(sid: number) {
        return await this.productStore.delete(sid);
    }
}