import {ProductService} from "./product.service";
import {Product} from "./product";
import {Router} from "express";

export class ProductRouter {
    private readonly _router:Router;

    constructor(private productService: ProductService) {
        this._router = Router();

        this.router.get("/", async (req, res, next) =>  {
            try {
                const products: Product[] = await this.productService.findAll();
                res.status(200).send(products);
            } catch (e: any) {
                next(e)
            }
        });

        this.router.get("/:sid",  async (req, res, next) => {
            const sid: number = parseInt(req.params.sid, 10);
            try {
                const product: Product = await this.productService.find(sid);
                if (product) {
                    return res.status(200).send(product);
                }
                res.status(404).json({error:"item not found"});
            } catch (e: any) {
                next(e);
            }
        });

        this.router.post("/", async (req, res, next) => {
            const product: Product = req.body;
            try {
                const newProduct = await this.productService.create(product);
                res.status(201).json(newProduct);
            } catch (e: any) {
                next(e);
            }
        });

        this.router.put("/:sid", async (req, res, next) => {
            const sid: number = parseInt(req.params.sid, 10);
            const exist: Product = await this.productService.find(sid);
            if(!exist) {
                res.status(404).json({error:"item not found"});
            } else {
                try {
                    const product: Product = req.body;
                    product.id = sid; // make sure the sid fits, alternative is returning an error.
                    let updProduct = await productService.update(product)
                    res.status(201).json(updProduct);
                } catch (e: any) {
                    next(e);
                }
            }
        });

        this.router.delete("/:sid", async(req, res, next) => {
            const sid: number = parseInt(req.params.sid, 10);
            const exist: Product = await this.productService.find(sid);
            if(!exist) {
                res.status(404).json({error:"item not found"});
            } else {
                try {
                    await productService.delete(sid);
                    res.status(204).json();
                } catch (e: any) {
                    next(e);
                }
            }
        });
    }

    public get router():Router{
        return this._router;
    }
}