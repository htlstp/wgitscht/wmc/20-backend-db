import {Product} from "../product";

export interface ProductStore {
    findAll():Promise<Product[]>;

    find(sid:number):Promise<Product>;

    put(product:Product):Promise<Product>;

    delete(sid:number):Promise<void>;

    update(product:Product):Promise<Product>;
}