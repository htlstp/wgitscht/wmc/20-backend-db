import {Product} from "../product";
import {ProductStore} from "./product.store";
import sql from 'mssql';
import {DBPool} from "../../dbPool";

export class ProductDbStore implements ProductStore {

    async delete(sid: number): Promise<void> {
        let result = await DBPool.getInstance().request()
            .input('id', sql.Int, sid)
            .query('delete from product where id = @id') // we use delete to show how it works, in a real world scenario might want to update a status
        if(result.rowsAffected[0] != 1) {
            throw new Error("failed to delete, no rows affected")
        }
    }

    async find(sid: number): Promise<Product> {
        let result = await DBPool.getInstance().request()
            .input('id', sql.Int, sid)
            .query<Product>('select * from product where id = @id')
        return result.recordset[0];
    }

    async findAll(): Promise<Product[]> {
        let result = await DBPool.getInstance().request()
            .query('select * from product')
        return result.recordset;
    }

    async update(product: Product): Promise<Product> {
        let result = await DBPool.getInstance().request()
            .input('sid', sql.Int, product.id)
            .input('name', sql.VarChar, product.name)
            .input('price', sql.Money, product.price)
            .query('UPDATE product SET name = @name, price = @price WHERE id = @sid');
        if(result.rowsAffected[0] !== 1) {
            throw new Error("failed to update, no rows affected")
        }
        return product;
    }

    async put(product: Product): Promise<Product> {
        let result = await DBPool.getInstance().request()
            .input('name',sql.VarChar, product.name)
            .input('price', sql.Money, product.price)
            .query('INSERT INTO PRODUCT (NAME, PRICE) VALUES (@name, @price); SELECT SCOPE_IDENTITY() AS id');

        product.id = result.recordset[0].id;
        return product
    }

}